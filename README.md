DSDaft
============

An homebrew for the Nintendo DS created by [corenting](http://www.corenting.fr).

It's a clone of [iDaft](http://www.najle.com/idaft/), a web and iPhone application.
