//Code from https://code.google.com/p/nds-diablo2-sorceress-project

#ifndef _SOUND_H
#define _SOUND_H
#include <maxmod9.h>
#include <stdio.h>

extern long streamLoaded;
mm_stream mystream;
mm_word stream(mm_word length, mm_addr dest, mm_stream_formats format);
bool initStream(char *rawfile);
void closeStream();

#endif // _SOUND_H
