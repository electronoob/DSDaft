//Code from https://code.google.com/p/nds-diablo2-sorceress-project

#include <nds.h>
#include <filesystem.h>
#include "sound.h"
#include "soundbank.h"
#include "soundbank_bin.h"

FILE *streamfile = 0;
long streamFileSize = 0;
long streamLoaded = 0;

bool initStream(char *rawfile)
{
    mystream.buffer_length = 1024;
    mystream.callback = stream;
    mystream.timer  = MM_TIMER0;
    mystream.manual = true;
    streamfile = fopen(rawfile, "rb");
    streamFileSize = 0;
    streamLoaded = 0;
    if (streamfile)
    {
        fseek(streamfile, 0, SEEK_END);
        streamFileSize = ftell(streamfile);
        fseek(streamfile, 0, SEEK_SET);
        mystream.sampling_rate = 16384;
        mystream.format = MM_STREAM_8BIT_STEREO;
        mmStreamOpen(&mystream);
        mmStreamUpdate();
        return 1;
    }
    return 0;
}

void closeStream()
{
    if (streamfile)
    {
        mmStreamClose();
        fclose(streamfile);
        streamLoaded = 0;
    }
}


mm_word stream(mm_word length, mm_addr dest, mm_stream_formats format)
{
    if (streamfile)
    {
        size_t samplesize = 0;
        switch (format)
        {
        case MM_STREAM_8BIT_MONO:
            samplesize = 1;
            break;
        case MM_STREAM_8BIT_STEREO:
            samplesize = 2;
            break;
        case MM_STREAM_16BIT_MONO:
            samplesize = 2;
            break;
        case MM_STREAM_16BIT_STEREO:
            samplesize = 4;
            break;
        }
        int res = 0;
        //test is we will not try to copy somthing out of the file... Seems that nitrofs allows reading farther than the length of the file
        if (streamFileSize < (streamLoaded + length * samplesize))length %= streamFileSize - streamLoaded;
        else res = fread(dest, samplesize, length, streamfile);
        if (res)
        {
            length = res;
        }
        else
        {
            closeStream();
            length = 0;
        }
        streamLoaded += length * samplesize;
    }
    return length;
}


