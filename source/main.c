/*
DSDaft
Corenting
www.corenting.fr
Version 2.1
*/

//Includes
#include <stdio.h>
#include <nds.h> //libnds
#include <filesystem.h>
#include <nf_lib.h> //nflib

//Maxmod
#include <maxmod9.h>
#include "soundbank.h"
#include "soundbank_bin.h"

//Others functions
#include "sound.h"
#include "functions.h" //splashscreen...

int main(int argc, char **argv)
{
    //NitroFS loading
    NF_Set2D(0, 0);
    NF_Set2D(1, 0);
    NF_SetRootFolder("NITROFS");
    swiWaitForVBlank();
    NF_Set2D(0, 0);
    NF_Set2D(1, 0);
    NF_InitTiledBgBuffers(); // bg buffer
    NF_InitTiledBgSys(0); // top bg
    NF_InitTiledBgSys(1); // bottom bg
    NF_InitTextSys(0);
    NF_InitTextSys(1);
    NF_LoadTextFont("fnt/default", "font_top", 256, 256, 0);
    NF_LoadTextFont("fnt/default", "font_bottom", 256, 256, 0);
    //Layers and color for the font
    NF_CreateTextLayer(0, 0, 0, "font_top");
    NF_CreateTextLayer(1, 0, 0, "font_bottom");
    NF_BgSetPalColor(0, 0, 1, 0, 31, 0);
    NF_BgSetPalColor(1, 0, 1, 0, 31, 0);
    mmInitDefaultMem((mm_addr)soundbank_bin); //load samples from maxmod

    //Vars
    touchPosition touch;
    int newpress;
    int x;
    int y;
    int beat = 0; //high = 0 and low = 1
    int chanson = 0; //none = 0 ; harder better faster stronger = 1 ; around the world = 2
    bool bg_music = false;

    //Samples
    mmLoadEffect(SFX_AFTER1);
    mmLoadEffect(SFX_BETTER1);
    mmLoadEffect(SFX_DOIT1);
    mmLoadEffect(SFX_EVER1);
    mmLoadEffect(SFX_FASTER1);
    mmLoadEffect(SFX_HARDER1);
    mmLoadEffect(SFX_HOUR1);
    mmLoadEffect(SFX_MAKEIT1);
    mmLoadEffect(SFX_MAKESUS1);
    mmLoadEffect(SFX_MORETHAN1);
    mmLoadEffect(SFX_NEVER1);
    mmLoadEffect(SFX_OUR1);
    mmLoadEffect(SFX_OVER1);
    mmLoadEffect(SFX_STRONGER1);
    mmLoadEffect(SFX_WORKIS1);
    mmLoadEffect(SFX_WORKIT1);
    mmLoadEffect(SFX_AFTER2);
    mmLoadEffect(SFX_BETTER2);
    mmLoadEffect(SFX_DOIT2);
    mmLoadEffect(SFX_EVER2);
    mmLoadEffect(SFX_FASTER2);
    mmLoadEffect(SFX_HARDER2);
    mmLoadEffect(SFX_HOUR2);
    mmLoadEffect(SFX_MAKEIT2);
    mmLoadEffect(SFX_MAKESUS2);
    mmLoadEffect(SFX_MORETHAN2);
    mmLoadEffect(SFX_NEVER2);
    mmLoadEffect(SFX_OUR2);
    mmLoadEffect(SFX_OVER2);
    mmLoadEffect(SFX_STRONGER2);
    mmLoadEffect(SFX_WORKIS2);
    mmLoadEffect(SFX_WORKIT2);
    mmLoadEffect(SFX_AROUND1);
    mmLoadEffect(SFX_AROUND2);

    //splash_screens("splashs/splash_haut", "splashs/splash_bas");

    //Menu
    NF_LoadTiledBg("bg/bg_haut", "bg_haut", 256, 256);
    NF_CreateTiledBg(0, 3, "bg_haut");
    NF_WriteText(0, 0, 19, 4, "2.1");
    if (NF_GetLanguage() == 2)
    {
        NF_WriteText(0, 0, 0, 22, "Par Corenting avec la           collaboration de Le_Rodeur");
    }
    else
    {
        NF_WriteText(0, 0, 0, 22, "By Corenting with contributions from Le_Rodeur");
    }

    //Main loop
    while (1)
    {
        while (chanson == 0)
        {
            // Touchscreen and keys scan
            scanKeys();
            touchRead(&touch);
            newpress = keysDown();
            x = touch.px;
            y = touch.py;
            NF_WriteText(1, 0, 14, 1, "Menu");
            NF_WriteText(1, 0, 0, 5, "Harder Better Faster Stronger");
            NF_WriteText(1, 0, 0, 10, "Around The World");
            NF_UpdateTextLayers();
            if (newpress && (x >= 0) && (y >= 35) && (x <= 255) && (y <= 55))
            {
                chanson = 1;
                NF_ClearTextLayer(1, 0);
                NF_WriteText(1, 0, 10, 23, "Play");
                NF_WriteText(1, 0, 26, 23, "Menu");
                NF_UpdateTextLayers();
                NF_LoadTiledBg("bg/harder_better_faster_stronger", "harder_better_faster_stronger", 256, 256);
                NF_CreateTiledBg(1, 3, "harder_better_faster_stronger");
            }
            if (newpress && (x >= 0) && (y >= 75) && (x <= 255) && (y <= 95))
            {
                chanson = 2;
                NF_ClearTextLayer(1, 0);
                NF_WriteText(1, 0, 2, 23, "Play");
                NF_WriteText(1, 0, 26, 23, "Menu");
                NF_UpdateTextLayers();
                NF_LoadTiledBg("bg/aroundtheworld", "aroundtheworld", 256, 256);
                NF_CreateTiledBg(1, 3, "aroundtheworld");
            }
        }

        while (chanson == 1)
        {
            scanKeys();
            touchRead(&touch);
            newpress = keysDown();
            x = touch.px;
            y = touch.py;

            if (newpress && (x >= 0) && (y >= 177) && (x <= 61) && (y <= 192))
            {
                if (beat == 1)
                {
                    beat = 0;
                }
                else
                {
                    beat = 1;
                }
            }

            if (beat == 1)
            {
                NF_WriteText(1, 0, 2, 23, "High");
            }
            else if (beat == 0)
            {
                NF_WriteText(1, 0, 2, 23, "Low ");
            }

            if (newpress && (x >= 196) && (y >= 177) && (x <= 256) && (y <= 192))
            {
                bg_music = false;
                NF_UnloadTiledBg("harder_better_faster_stronger");
                NF_DeleteTiledBg(1, 3);
                chanson = 0;
                beat = 0;
                closeStream();
                mmEffectCancelAll();
                NF_ClearTextLayer(1, 0);
            }


            if (newpress && (x >= 66) && (y >= 177) && (x <= 125) && (y <= 192))
            {
                if (streamLoaded == 0)
                {
                    bg_music = true;
                    initStream("harder_background.raw");
                    NF_WriteText(1, 0, 10, 23, "Stop");
                    NF_UpdateTextLayers(); // Actualisation du texte
                }
                else
                {
                    bg_music = false;
                    NF_WriteText(1, 0, 10, 23, "Play");
                    NF_UpdateTextLayers(); // Actualisation du texte
                    closeStream();
                }
            }
            if (newpress && (x >= 0) && (y >= 0) && (x <= 58) && (y <= 35))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_WORKIT1);
                }
                else
                {
                    mmEffect(SFX_WORKIT2);
                }
            }
            if (newpress && (x >= 66) && (y >= 0) && (x <= 123) && (y <= 35))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_MAKEIT1);
                }
                else
                {
                    mmEffect(SFX_MAKEIT2);
                }
            }
            if (newpress && (x >= 132) && (y >= 0) && (x <= 187) && (y <= 35))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_DOIT1);
                }
                else
                {
                    mmEffect(SFX_DOIT2);
                }
            }
            if (newpress && (x >= 197) && (y >= 0) && (x <= 255) && (y <= 35))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_MAKESUS1);
                }
                else
                {
                    mmEffect(SFX_MAKESUS2);
                }
            }
            if (newpress && (x >= 0) && (y >= 45) && (x <= 58) && (y <= 80))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_HARDER1);
                }
                else
                {
                    mmEffect(SFX_HARDER2);
                }
            }
            if (newpress && (x >= 67) && (y >= 45) && (x <= 122) && (y <= 80))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_BETTER1);
                }
                else
                {
                    mmEffect(SFX_BETTER2);
                }
            }
            if (newpress && (x >= 133) && (y >= 45) && (x <= 189) && (y <= 80))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_FASTER1);
                }
                else
                {
                    mmEffect(SFX_FASTER2);
                }
            }
            if (newpress && (x >= 197) && (y >= 45) && (x <= 252) && (y <= 80))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_STRONGER1);
                }
                else
                {
                    mmEffect(SFX_STRONGER2);
                }
            }
            if (newpress && (x >= 0) && (y >= 89) && (x <= 58) && (y <= 125))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_MORETHAN1);
                }
                else
                {
                    mmEffect(SFX_MORETHAN2);
                }
            }
            if (newpress && (x >= 67) && (y >= 89) && (x <= 122) && (y <= 125))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_HOUR1);
                }
                else
                {
                    mmEffect(SFX_HOUR2);
                }
            }
            if (newpress && (x >= 132) && (y >= 89) && (x <= 190) && (y <= 125))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_OUR1);
                }
                else
                {
                    mmEffect(SFX_OUR2);
                }
            }
            if (newpress && (x >= 198) && (y >= 89) && (x <= 253) && (y <= 125))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_NEVER1);
                }
                else
                {
                    mmEffect(SFX_NEVER2);
                }
            }
            if (newpress && (x >= 0) && (y >= 132) && (x <= 57) && (y <= 168))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_EVER1);
                }
                else
                {
                    mmEffect(SFX_EVER2);
                }
            }
            if (newpress && (x >= 66) && (y >= 132) && (x <= 123) && (y <= 168))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_AFTER1);
                }
                else
                {
                    mmEffect(SFX_AFTER2);
                }
            }
            if (newpress && (x >= 132) && (y >= 132) && (x <= 187) && (y <= 168))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_WORKIS1);
                }
                else
                {
                    mmEffect(SFX_WORKIS2);
                }
            }
            if (newpress && (x >= 197) && (y >= 132) && (x <= 253) && (y <= 168))
            {
                if (beat == 0)
                {
                    mmEffect(SFX_OVER1);
                }
                else
                {
                    mmEffect(SFX_OVER2);
                }
            }

            if (streamLoaded != 0)
            {
                mmStreamUpdate();
            }

            loop_stream("harder_background.raw", streamLoaded, bg_music);
            NF_UpdateTextLayers();
            swiWaitForVBlank();
        }

        while (chanson == 2)
        {
            loop_stream("around_bg.raw", streamLoaded, bg_music);
            scanKeys();
            touchRead(&touch);
            newpress = keysDown();
            x = touch.px;
            y = touch.py;

            if (newpress && (x >= 193) && (y >= 177) && (x <= 252) && (y <= 192))
            {
                bg_music = false;
                NF_UnloadTiledBg("aroundtheworld");
                NF_DeleteTiledBg(1, 3);
                chanson = 0;
                beat = 0;
                closeStream();
                mmEffectCancelAll();
                NF_ClearTextLayer(1, 0);
            }

            if (newpress && (x >= 5) && (y >= 19) && (x <= 251) && (y <= 91))
            {
                mmEffect(SFX_AROUND1);
            }

            if (newpress && (x >= 5) && (y >= 99) && (x <= 251) && (y <= 171))
            {
                mmEffect(SFX_AROUND2);
            }

            if (newpress && (x >= 5) && (y >= 177) && (x <= 64) && (y <= 192))
            {
                if (streamLoaded == 0)
                {
                    bg_music = true;
                    initStream("around_bg.raw");
                    NF_WriteText(1, 0, 2, 23, "Stop");

                }
                else
                {
                    bg_music = false;
                    NF_WriteText(1, 0, 2, 23, "Play");
                    closeStream();
                }

            }

            if (streamLoaded != 0)
            {
                mmStreamUpdate();
            }

            NF_UpdateTextLayers();
            swiWaitForVBlank();
        }

    }
    return 0;
}
