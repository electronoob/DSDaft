//Misc functions

#include <stdio.h>
#include <nds.h> //libnds
#include <filesystem.h>
#include <nf_lib.h> //nflib
#include <maxmod9.h>
#include "sound.h"


void splash_screens(char *top, char *bottom)
{
    int i;
    int newpress = keysDown();

    //Chargement splash
    NF_LoadTiledBg(top, "splash_haut", 256, 256);
    NF_LoadTiledBg(bottom, "splash_bas", 256, 256);
    NF_CreateTiledBg(0, 3, "splash_haut");
    NF_CreateTiledBg(1, 3, "splash_bas");

    for (i = -80; i == 0; i++)
    {
        setBrightness(3, i / 4);
        swiWaitForVBlank();
    }

    for (i = 0; i <= 60; i++)
    {
        scanKeys();
        if (newpress) break;
        swiWaitForVBlank();

    }

    for (i = 0; i == -80; i--)
    {
        setBrightness(3, i / 4);
        swiWaitForVBlank();
    }

    //Fin splashscreens
    setBrightness(3, 0);    //Luminosité normale
    //Suppression de la RAM et de l'affichage des splash
    NF_UnloadTiledBg("splash_haut");
    NF_UnloadTiledBg("splash_bas");
    NF_DeleteTiledBg(1, 3);
    NF_DeleteTiledBg(0, 3);
}

bool loop_stream(char *name, long loaded, bool want_music)
{
    if (loaded <= 0 && want_music)
    {
        closeStream();
        initStream(name, 16384);
        mmStreamUpdate();
    }
}